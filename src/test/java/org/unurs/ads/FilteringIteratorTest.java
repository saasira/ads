/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.unurs.ads;

import java.lang.reflect.Field;
import java.util.HashSet;
import java.util.NoSuchElementException;
import java.util.Set;
import java.util.concurrent.ThreadLocalRandom;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 * @author Samba Kolusu
 * @email saasira@live.com
 * @email saasira@gmail.com
 **/
public class FilteringIteratorTest {
    
    public static final String ALNUM = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
    private static Set<String> DATA=new HashSet<>();
    
    public static String random( int length ) {
        int L=ALNUM.length();
        ThreadLocalRandom random=ThreadLocalRandom.current();
        StringBuilder sb = new StringBuilder( length );
        for( int i = 0; i < length; i++ ) {
            sb.append( ALNUM.charAt( random.nextInt(L) ) );
        }
        return sb.toString();
    }
    
    public FilteringIteratorTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
        
        for(int counter=0;counter<1000;counter++) {
            DATA.add(random( ( (int) ( Math.random() )*10 ) +10 ) );
        }
        
    }
    
    @AfterClass
    public static void tearDownClass() {
        DATA.clear();
        DATA=null;
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Confirm that calling hasNext repeatedly will not advance the iterator
     **/
    @Test
    public void testHasNext() {
        System.out.print("hasNext : ");
        
        FilteringIterator iterator=new FilteringIterator(DATA.iterator(), new HashcodeFilter());
        Object expected=null;
        Object outcome=null;
        
        int original=0;
        for(Object o : DATA) {
            if(o.hashCode()>Short.MAX_VALUE) {
                original++;
            }
        }
        
        iterator.hasNext();
        expected=getFieldValue(iterator,"next");
        
        if(original > 0) {
            assertNotNull(expected);// may be not of much significance but nevertheless asserts that we have at least one filtered element
        }
        
        for(int i=0; i < ( 2* DATA.size() ); i++) {//confirm that we don't get no such element exception even if were to supposedly iterate past the size of the collection since calling hasNext repeatedly will not advance the iterator
            iterator.hasNext();
            outcome=getFieldValue(iterator,"next");
            assertEquals(expected, outcome);
        }
        
    }

    /**
     * Test of next method, of class FilteringIterator.
     */
    @Test
    public void testNext() {
        System.out.print("next : ");
        int original=0;
        for(Object o : DATA) {
            if(o.hashCode()>Short.MAX_VALUE) {
                original++;
            }
        }
        
        FilteringIterator iterator=new FilteringIterator(DATA.iterator(), new HashcodeFilter());
        Object result;
        int filtered=0;
        while(iterator.hasNext()) {
            result=iterator.next();
            filtered++;
        }
        assertEquals(original, filtered);
        System.out.println("Original : "+ original+" ;  Filtered :" + filtered);
    }

    /**
     * confirm that calling next without calling hasNext will internally call hasNext and return the available entry or throw no such element exception
     **/
    @Test
    public void testRepeatNext() {
        FilteringIterator iterator=new FilteringIterator(DATA.iterator(), new HashcodeFilter());
        Object expected=null;
        Object outcome=null;
        
        expected=iterator.next();
        NoSuchElementException exception=null;
        
        for(int i=0; i< (DATA.size()+100); i++) {//iterating past the size of the collection to ensure that we throw NoSuchElementException if we iterate past the last element in the collection
            try {
                outcome=iterator.next();
                assertNotSame(expected, outcome);
            } catch(NoSuchElementException nsee) {
                exception=nsee;
            }
        }
        assertNotNull(exception);
    }
    
    /**
     * Test of remove method, of class FilteringIterator.
     */
    @Test
    public void testRemove() {
        System.out.print("remove : ");
        
        int original=0;
        for(Object o : DATA) {
            if(o.hashCode()>Short.MAX_VALUE) {
                original++;
            }
        }
        
        FilteringIterator iterator=new FilteringIterator(DATA.iterator(), new HashcodeFilter());
        Object result;
        int filtered=0;
        while(iterator.hasNext()) {
            result=iterator.next();
            iterator.remove();
            filtered++;
        }
        assertEquals(original, filtered);
        
        System.out.println("Original : "+ original+" ;  Filtered :" + filtered);

    }
    
    private Object getFieldValue(Object object, String fieldName) {
        Object value=null;
        try {
            Field field = object.getClass().getDeclaredField(fieldName); 
            field.setAccessible(true);
            value= field.get(object);
        } catch (NoSuchFieldException | SecurityException | IllegalArgumentException | IllegalAccessException ex) {
            Logger.getLogger(FilteringIteratorTest.class.getName()).log(Level.SEVERE, null, ex);
        }
        return value;
    }
}
