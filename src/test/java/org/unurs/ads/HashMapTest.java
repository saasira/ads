/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.unurs.ads;

import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ThreadLocalRandom;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 * @author Samba Kolusu
 * @email saasira@live.com
 * @email saasira@gmail.com
 **/
public class HashMapTest {
    
    public static final String ALNUM = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
    
    public static String random( int length ) {
        int L=ALNUM.length();
        ThreadLocalRandom random=ThreadLocalRandom.current();
        StringBuilder sb = new StringBuilder( length );
        for( int i = 0; i < length; i++ ) {
            sb.append( ALNUM.charAt( random.nextInt(L) ) );
        }
        return sb.toString();
    }
    
    private static final Map<String,Long> map=new HashMap<>();
    
    public HashMapTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of size method, of class HashMap.
     */
    @Test
    public void testSize() {
        System.out.println("size");
        int oldSize=map.size();
        int expResult = oldSize+1;
        map.put("size", 1L);
        int result = map.size();
        assertEquals(expResult, result);
    }

    /**
     * Test of isEmpty method, of class HashMap.
     */
    @Test
    public void testIsEmpty() {
        System.out.println("isEmpty");
        
        map.put("isEmpty",3L);
        boolean expResult = false;
        boolean result = map.isEmpty();
        assertEquals(expResult, result);
        
        map.clear();
        expResult = true;
        result = map.isEmpty();
        assertEquals(expResult, result);
    }

    /**
     * Test of containsKey method, of class HashMap.
     */
    @Test
    public void testContainsKey() {
        System.out.println("containsKey");
        String key = "containsKey";
        Long value=13L;
        map.put(key, value);
        boolean expResult = true;
        boolean result = map.containsKey(key);
        assertEquals(expResult, result);
        
        result=map.containsKey("stupid");
        expResult=false;
        assertEquals(expResult, result);
    }

    /**
     * Test of containsValue method, of class HashMap.
     */
    //@Test
    public void testContainsValue() {
        System.out.println("containsValue");
        Object value = null;
        boolean expResult = false;
        boolean result = map.containsValue(value);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of get method, of class HashMap.
     */
    @Test
    public void testGet() {
        System.out.println("get");
        String key = "get";
        Long value=7L;
        Long expResult = value;
        map.put(key,value);
        Long result = map.get(key);
        assertEquals(expResult, result);
    }

    /**
     * Test of put method, of class HashMap.
     */
    @Test
    public void testPut() {
        System.out.println("put");
        String key = "put";
        Long value = 12L;
        Long expResult = null;
        int oldSize=map.size();
        Long result = map.put(key, value);
        int newSize=map.size();
        assertTrue(newSize==oldSize+1);
        assertEquals(expResult, result);
        
        expResult=value;
        result=map.get(key);
        assertEquals(expResult, result);
    }

    /**
     * Test of remove method, of class HashMap.
     */
    @Test
    public void testRemove() {
        System.out.println("remove");
        String key = "remove";
        Long value=10L;
        Long expResult = 10L;
        int oldSize=map.size();
        map.put(key, value);
        int newSize=map.size();
        assertTrue(newSize==oldSize+1);
        Long result = map.remove(key);
        int latestSize=map.size();
        assertEquals(expResult, result);
        assertTrue(latestSize==oldSize);
    }

    /**
     * Test of putAll method, of class HashMap.
     */
    @Test
    public void testPutAll() {
        System.out.println("putAll");
        Map<String,Long> m = new java.util.HashMap();
        m.put("Samba", 1L);
        m.put("Siva",2L);
        m.put("Rao", 3L);
        int oldSize=map.size();
        map.putAll(m);
        int newSize=map.size();
        assertTrue( newSize == (oldSize+m.size()) );
    }

    @Test
    public void testResize() {
        map.clear();
        
        String[] keys=new String[1000];
        String key;
        
        //test for resizing on put
        for(long index=0;index<1000;index++) {
            key=random(16);
            map.put(key,index);
            keys[(int)index]=key;
        }
        
        Long value;
        for(long index=0;index<25;index++) {
            key=keys[(int)index];
            value=map.get(key);
            assertEquals(Long.valueOf(index), value);
        }
        
        //test for resising on removeAll
        int oldSize=map.size();
        List<String> keySet=Arrays.asList(Arrays.copyOfRange(keys, 0, 600));
        map.keySet().removeAll(keySet);
        int newSize=map.size();
        assertEquals(oldSize,newSize+600);
    }
    
    /**
     * Test of clear method, of class HashMap.
     */
    @Test
    public void testClear() {
        System.out.println("clear");
        map.put("test", 123L);
        int oldSize=map.size();
        assertTrue(oldSize!=0);
        map.clear();
        int newSize=map.size();
        assertTrue(newSize==0);
    }

    /**
     * Test of keySet method, of class HashMap.
     */
    @Test
    public void testKeySet() {
        System.out.println("keySet");
        map.clear();
        
        
        String key;
        String[] keys=new String[1000];
        for(long index=0;index<1000;index++) {
            key=random(16);
            map.put(key,index);
            keys[(int)index]=key;
        }
        
        Set<String> keySet= map.keySet();
        Iterator<String> iterator=keySet.iterator();
        Long value;
        int index=0;
        while(iterator.hasNext()) {
            key=iterator.next();
            value=map.get(key);
            if(value!=null) {
                assertEquals(key, keys[Long.valueOf(value).intValue()]);
            }
            assertTrue(value!=null);//only because we know we did not add null values
        }
    }

    /**
     * Test of values method, of class HashMap.
     */
    @Test
    public void testValues() {
        map.clear();
        
        
        String key;
        String[] keys=new String[1000];
        for(long index=0;index<1000;index++) {
            key=random(16);
            map.put(key,index);
            keys[(int)index]=key;
        }
        
        Collection<Long> values= map.values();
        Iterator<Long> iterator=values.iterator();
        Long result;
        Long expected;
        
        while(iterator.hasNext()) {
            result=iterator.next();
            if(result!=null) { // can have null values
                expected=map.get(keys[Long.valueOf(result).intValue()]);
                assertEquals(expected, result);
            }
            assertTrue(result!=null);//only because we know we did not add null values
        }
    }

    /**
     * Test of entrySet method, of class HashMap.
     */
    @Test
    public void testEntrySet() {
        System.out.println("entrySet");
        map.clear();
        
        
        String key;
        String[] keys=new String[1000];
        for(long index=0;index<1000;index++) {
            key=random(16);
            map.put(key,index);
            keys[(int)index]=key;
        }
        
        Set<Map.Entry<String,Long>> keySet= map.entrySet();
        Iterator<Map.Entry<String,Long>> iterator=keySet.iterator();
        Long value;
        Map.Entry<String,Long> entry;
        while(iterator.hasNext()) {
            entry=iterator.next();
            key=entry.getKey();
            value=map.get(key);
            if(value!=null) {
                assertEquals(key, keys[Long.valueOf(value).intValue()]);
            }
            assertTrue(value!=null);//only because we know we did not add null values
        }
    }
    
    @Test
    public void testPerformance() {
        map.clear();
        
        long begin, end;
        int size=1000000;
        
        String key;
        Long value;
        
        begin=System.currentTimeMillis();
        //test for resizing on put
        String[] keys=new String[size];
        for(long index=0;index<size;index++) {
            key=random(16);
            map.put(key,index);
            keys[(int)index]=key;
        }
        end=System.currentTimeMillis();
        System.out.println("Time taken by our HashMap to populate : "+(end-begin) + " milliseconds");
        
        begin=System.currentTimeMillis();
        for(long index=0;index<size;index++) {
            key=keys[(int)index];
            value=map.get(key);
            assertEquals(Long.valueOf(index), value);
        }
        end=System.currentTimeMillis();
        System.out.println("Time taken by our HashMap to retrieve : "+(end-begin) + " milliseconds");
        
        begin=System.currentTimeMillis();
        List<String>  keySet=Arrays.asList(Arrays.copyOfRange(keys, 9000, 90000));
        map.keySet().removeAll(keySet);
        end=System.currentTimeMillis();
        System.out.println("Time taken by our HashMap to remove : "+(end-begin) + " milliseconds");
        
        
        //HashMap tests
        Map<String,Long> hmap=new java.util.HashMap<>();
        begin=System.currentTimeMillis();
        //test for resizing on put
        keys=new String[size];
        for(long index=0;index<size;index++) {
            key=random(16);
            hmap.put(key,index);
            keys[(int)index]=key;
        }
        end=System.currentTimeMillis();
        System.out.println("Time taken by java.util.HashMap to populate : "+(end-begin) + " milliseconds");
        
        begin=System.currentTimeMillis();
        for(long index=0;index<size;index++) {
            key=keys[(int)index];
            value=hmap.get(key);
            assertEquals(Long.valueOf(index), value);
        }
        end=System.currentTimeMillis();
        System.out.println("Time taken by java.util.HashMap to retrieve : "+(end-begin) + " milliseconds");
        
        //test for resising on removeAll
        begin=System.currentTimeMillis();
        keySet=Arrays.asList(Arrays.copyOfRange(keys, 9000, 90000));
        hmap.keySet().removeAll(keySet);
        end=System.currentTimeMillis();
        System.out.println("Time taken by java.util.HashMap to remove : "+(end-begin) + " milliseconds");
        
    }
    
    public void testChangingInitialSize() {
    
    }
    
    public void testChangingIncrement() {
    
    }
    
    public void testChangingInitialSizeAndIncrement() {
    
    }
}
