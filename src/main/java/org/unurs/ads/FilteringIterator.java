/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.unurs.ads;

import java.util.Iterator;
import java.util.NoSuchElementException;

/**
 * NOTE : Not Thread Safe
 * @author Samba Kolusu
 * @email saasira@live.com
 * @email saasira@gmail.com
 */
public class FilteringIterator implements Iterator {
    private final Iterator iterator;
    private final Filter filter;
    
    private Object next;
    
    private boolean more=false;
        
    public FilteringIterator(Iterator iterator, Filter filter) {
        this.iterator = iterator;
        this.filter = filter;
    }

    @Override
    public boolean hasNext() {
        if(more) {// hasNext is called again without calling next
            return true;
        } else {
            Object test;
            while(iterator.hasNext()) {
                test=iterator.next();
                if(filter.apply(test)) {
                    next=test;
                    more=true;
                    break;
                }
            }
        }
        
        return more;
    }

    @Override
    public Object next() {
        if(more) { // hasNext has been called before
            more=false;
            return next;
        } else if(hasNext()) { // hasNext was not called before, so we call it now
            more=false;
            return next;
        }  else { // hasNext returned false, and set next=null; so we throw no more elements exception
            throw new NoSuchElementException("no more elements matching the filter criteria");
        }
    }

    @Override
    public void remove() {
        iterator.remove();
    }
    
    
}
