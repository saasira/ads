/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.unurs.ads;

import java.lang.reflect.Array;
import java.util.Collection;
import java.util.Iterator;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

/**
 * @author Samba Kolusu
 * @email saasira@live.com
 * @email saasira@gmail.com
 */

/**
 * <b> Performance Comparison Statistics: </b>
 Time taken by HashMap to populate : 2836 milliseconds
 Time taken by HashMap to retrieve : 402 milliseconds
 Time taken by HashMap to remove : 64 milliseconds
 Time taken by HashMap to populate : 2545 milliseconds
 Time taken by HashMap to retrieve : 272 milliseconds
 Time taken by HashMap to remove : 36 milliseconds
 * 
 * @param <K>   the key class type
 * @param <V>   the value class type
 **/
public class HashMap<K, V> implements Map<K, V> {

    private Element[] elements;

    private int initialSize = 25;
    private float increment = 1.5f;

    private int size;

    public HashMap() {
        this.elements = new HashMap.Element[this.initialSize];
    }

    public HashMap(int initialSize) {
        this.initialSize = initialSize;
        this.elements = new HashMap.Element[this.initialSize];
    }

    public HashMap(int initialSize, float increment) {
        this.initialSize = initialSize;
        this.increment = increment;
        this.elements = new HashMap.Element[this.initialSize];
    }

    public final class Element implements Entry<K, V> {

        private final K key;
        private V value;

        private Element next;

        public Element(K key, V value) {
            this.key = key;
            this.value = value;
        }

        @Override
        public K getKey() {
            return key;
        }

        @Override
        public V getValue() {
            return value;
        }

        @Override
        public V setValue(V value) {
            V oldValue = this.value;
            this.value = value;
            return oldValue;
        }

        @Override
        public int hashCode() {
            int hash = 7;
            hash = 83 * hash + Objects.hashCode(this.key);
            hash = 83 * hash + Objects.hashCode(this.value);
            return hash;
        }

        @Override
        public boolean equals(Object obj) {
            if (obj == null) {
                return false;
            }
            if (getClass() != obj.getClass()) {
                return false;
            }
            final Element other = (Element) obj;
            if (!Objects.equals(this.key, other.key)) {
                return false;
            }
            if (!Objects.equals(this.value, other.value)) {
                return false;
            }
            return true;
        }

    }

    private void resizeIfNeededForPut(int need) {
        
        if ( size+need > Integer.MAX_VALUE) { // we don't store more than Integer.MAX_VALUE number of elements even though we may be able to store more elements due to less than optimum hashcode
            throw new IndexOutOfBoundsException("exceeded the storage capacity of this map");
        }
        
        int expected =  Math.min( Double.valueOf( (elements.length+need) * increment).intValue(), Integer.MAX_VALUE);   // ensue we never exceed the expected value greater than Integer.MAX_VALUE as otherwise we may get ArrayIndexOutOfBoundsException
        
        if (size + need > elements.length) { //size is the number of elements present in the array, while  elements.length is capacity
            Element[] temp = elements;
            elements = new HashMap.Element[ expected ];
            reindex(temp, elements);
        }
    }
    
    private void resizeIfNeededForRemove(int need) {
        if(elements.length-need < 0 ) {
            throw new IndexOutOfBoundsException("not enough elements to be removed");
        }
        
        if( size-need < (elements.length/2)  ) {// looks this division by 2 is sensitive to the initialSize and increment; find a better divisor that is immune to changing these two factors
            Element[] temp = elements;
            elements = new HashMap.Element[(int) (elements.length / increment)];
            reindex(temp, elements);
        }
    }
    
    private void reindex(Element[] previous, Element[] current) {
        for (Element e : previous) {
            if (e != null) { // we may have some blank positions in the array
                int hash = hash(e.key);
                int index = getIndex(hash, current.length);//current.length is capacity
                addElement(current, e, index);

                while (e.next != null) {
                    e = e.next;

                    hash = hash(e.key);
                    int newIndex = getIndex(hash, current.length);

                    if (newIndex != index) { //new index is different from this index
                        addElement(current, e, newIndex);
                    } else { // newindex is same as this index
                        addElement(current, e, index);
                    }
                }
            }
        }
    }

    private static int hash(Object o) {
        int h = Objects.hashCode(o);
        return (h & 0x7fffffff);
    }

    /**
     * Returns index for hash code h.
     */
    private static int getIndex(int hash, int length) {
        int index;
        if (length == 0) {
            return 0;
        } else {
            index = hash % length;
        }
        return index;
    }

    private void addElement(Element[] collection, Element e, int index) {
        HashMap.Element t = collection[index];
        if (t == null) {
            collection[index] = new Element(e.key, e.value); //put it in a new bucket
        } else {
            while (t.next != null) {
                t = t.next;
            }
            t.next = new Element(e.key, e.value);   //append to the chain of elements in an existing bucket
        }
    }

    private Element getElement(Object key) {
        int hash = hash(key);
        int index = getIndex(hash, elements.length);
        Element e = elements[index];
        // if bucket is found then traverse through the linked list and
        // see if element is present
        while (e != null) {
            if (e.getKey().equals(key)) {
                break;
            }
            e = e.next;
        }
        return e;
    }

    ////////////////////////////////////////////////////////////////////////////
    //  Implementation of public methods as mandated by Map<K,V> interface  ////
    ////////////////////////////////////////////////////////////////////////////
    @Override
    public int size() {
        return this.size;
    }

    @Override
    public boolean isEmpty() {
        return this.size == 0;
    }

    @Override
    public boolean containsKey(Object key) {
        Element e = getElement(key);
        return e != null;
    }

    @Override
    public boolean containsValue(Object value) {
        for (Element element : elements) {
            for (Element e = element; e != null; e = e.next) {
                if (value != null && value.equals(e.value)) {
                    return true;
                }
                if (value == null && e.value == null) {
                    return true;
                }
            }
        }
        return false;
    }

    @Override
    public V get(Object key) {
        V value = null;
        Element e = getElement(key);
        if (e != null) {
            value = e.getValue();
        }
        return value;
    }

    @Override
    public V put(K key, V value) {
        resizeIfNeededForPut(1);//ensure we have sufficient space to add one more element
        int hash = hash(key);
        int index = getIndex(hash, elements.length); //elements.length=capacity
        Element e = elements[index];
        V oldValue = null;
        if (e != null) {
            // it means we are trying to update the key-value pair, 
            //hence overwrite the the old k-v pair with the new k-v pair
            if (e.key.equals(key)) {
                oldValue = e.value;
                e.value = value;//update
            } else {
                // traverse to the end of the list and insert new element
                // in the same bucket
                while (e.next != null) {
                    e = e.next;
                }
                //add the new element in the existing bucket
                Element element = new Element(key, value);
                e.next = element;
            }
        } else {
            // add the new element in a new bucket
            Element element = new Element(key, value);
            elements[index] = element;
        }

        this.size++;
        return oldValue;
    }

    @Override
    public V remove(Object key) {
        int hash = hash(key);
        int index = getIndex(hash, elements.length);
        Element e = elements[index];
        // if bucket is found then traverse through the linked list and
        // see if element is present
        Element previous = null;
        V deletedValue = null;
        while (e != null) {
            // if element is found
            if (e.getKey().equals(key) && hash(key) == hash) {
                if (previous == null) {
                    previous = e.next; //if the element we are removing is the first one, then make the next element as the firrst
                } else {
                    previous.next = e.next; // else connect the element previous to the one we are removing with the element next to the one we are removing
                }
                deletedValue = e.value;
                elements[index] = previous;
                break;
            }
            previous = e;
            e = e.next;
        }
        this.size--;
        resizeIfNeededForRemove(1);
        return deletedValue;
    }

    @Override
    public void putAll(Map<? extends K, ? extends V> m) {
        if (m == null || m.isEmpty()) {
            return;
        }

        resizeIfNeededForPut(m.size()); // ensure we have sufficient space to add m.size() elements in this map
        for (Map.Entry<? extends K, ? extends V> element : m.entrySet()) {
            put(element.getKey(), element.getValue());
        }
    }

    @Override
    public void clear() {
        this.elements = null; // free up the elements for garbage collection
        this.elements = new HashMap.Element[this.initialSize];
        this.size = 0;
    }

    @Override
    public Set<K> keySet() {
        return new KeySet();
    }

    @Override
    public Collection<V> values() {
        return new ValueSet();
    }

    @Override
    public Set<Entry<K, V>> entrySet() {
        return new ElementSet();
    }

    ////////////////////////////////////////////////////////////////////////////
    //  Implementation of EntrySet, KeySet, ValueSet and their iterators    ////
    ////////////////////////////////////////////////////////////////////////////
    public final class ElementSet implements Set<Entry<K, V>> {

        @Override
        public int size() {
            return HashMap.this.size;
        }

        @Override
        public boolean isEmpty() {
            return HashMap.this.isEmpty();
        }

        @Override
        public boolean contains(Object o) {
            Element element = (Element) o;
            return HashMap.this.containsKey(element.key);
        }

        @Override
        public Iterator<Entry<K, V>> iterator() {
            return new ElementIterator();
        }

        @Override
        public Object[] toArray() {
            Object[] entries = new Object[size]; //don't call it elements since the actual array is called elements
            int index = 0;
            for (Element e : elements) {
                e = elements[index];
                entries[index] = e;
                int i = 0;
                while (e.next != null) {
                    i++;
                    entries[index + i] = e.next;
                    e = e.next;
                }
                index += i;
            }
            return entries;
        }

        @Override
        public <T> T[] toArray(T[] a) {
            T[] entries = (T[]) Array.newInstance(a.getClass().getComponentType(), size);
            int index = 0;
            for (Element e : elements) {
                e = elements[index];
                entries[index] = (T) e;
                int i = 0;
                while (e.next != null) {
                    i++;
                    entries[index + i] = (T) e.next;
                    e = e.next;
                }
                index += i;
            }
            return entries;
        }

        @Override
        public boolean add(Entry<K, V> e) {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }

        @Override
        public boolean remove(Object o) {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }

        @Override
        public boolean containsAll(Collection<?> c) {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }

        @Override
        public boolean addAll(Collection<? extends Entry<K, V>> c) {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }

        @Override
        public boolean retainAll(Collection<?> c) {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }

        @Override
        public boolean removeAll(Collection<?> c) {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }

        @Override
        public void clear() {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }

    }

    public final class KeySet implements Set<K> {

        @Override
        public int size() {
            return HashMap.this.size;
        }

        @Override
        public boolean isEmpty() {
            return HashMap.this.isEmpty();
        }

        @Override
        public boolean contains(Object o) {
            K key = (K) o;
            return HashMap.this.containsKey(key);
        }

        @Override
        public Iterator<K> iterator() {
            return new KeyIterator();
        }

        @Override
        public Object[] toArray() {
            Object[] keys = new Object[size];
            int index = 0;
            for (Element e : elements) {
                e = elements[index];
                keys[index] = e.key;
                int i = 0;
                while (e.next != null) {
                    i++;
                    keys[index + i] = e.next.key;
                    e = e.next;
                }
                index += i;
            }
            return keys;
        }

        @Override
        public <T> T[] toArray(T[] a) {
            T[] keys = (T[]) Array.newInstance(a.getClass().getComponentType(), size);
            int index = 0;
            for (Element e : elements) {
                e = elements[index];
                keys[index] = (T) e.key;
                int i = 0;
                while (e.next != null) {
                    i++;
                    keys[index + i] = (T) e.next.key;
                    e = e.next;
                }
                index += i;
            }
            return keys;
        }

        @Override
        public boolean add(K e) {
            throw new UnsupportedOperationException("Not applicable"); 
        }

        @Override
        public boolean remove(Object o) {
            boolean present=HashMap.this.containsKey((K)o);
            HashMap.this.remove((K)o);
            return present;  // NOTE: not acccurate, the value stored in the map for this key may also be null
        }

        @Override
        public boolean containsAll(Collection<?> c) {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }

        @Override
        public boolean addAll(Collection<? extends K> c) {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }

        @Override
        public boolean retainAll(Collection<?> c) {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }

        @Override
        public boolean removeAll(Collection<?> collection) {
            boolean changed=false;
            int count=0;
            for(Object key : collection) {
                changed=this.remove((K)key);
                if(changed) {
                    count++;
                }
            }
            resizeIfNeededForRemove(count);
            return changed;
        }

        @Override
        public void clear() {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }

    }

    public final class ValueSet implements Set<V> {

        @Override
        public int size() {
            return HashMap.this.size;
        }

        @Override
        public boolean isEmpty() {
            return HashMap.this.isEmpty();
        }

        @Override
        public boolean contains(Object o) {
            V value = (V) o;
            return HashMap.this.containsValue(value);
        }

        @Override
        public Iterator<V> iterator() {
            return new ValueIterator();
        }

        @Override
        public Object[] toArray() {
            Object[] values = new Object[size];
            int index = 0;
            for (Element e : elements) {
                e = elements[index];
                values[index] = e.value;
                int i = 0;
                while (e.next != null) {
                    i++;
                    values[index + i] = e.next.value;
                    e = e.next;
                }
                index += i;
            }
            return values;
        }

        @Override
        public <T> T[] toArray(T[] a) {
            T[] values = (T[]) Array.newInstance(a.getClass().getComponentType(), size);
            int index = 0;
            for (Element e : elements) {
                e = elements[index];
                values[index] = (T) e.value;
                int i = 0;
                while (e.next != null) {
                    i++;
                    values[index + i] = (T) e.next.value;
                    e = e.next;
                }
                index += i;
            }
            return values;
        }

        @Override
        public boolean add(V e) {
            throw new UnsupportedOperationException("Not applicable"); 
        }

        @Override
        public boolean remove(Object o) {
            throw new UnsupportedOperationException("Not applicable"); //To change body of generated methods, choose Tools | Templates.
        }

        @Override
        public boolean containsAll(Collection<?> c) {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }

        @Override
        public boolean addAll(Collection<? extends V> c) {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }

        @Override
        public boolean retainAll(Collection<?> c) {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }

        @Override
        public boolean removeAll(Collection<?> c) {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }

        @Override
        public void clear() {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }

    }

    private final class ElementIterator implements Iterator<Entry<K, V>> {

        private int index = 0;
        private int counter = 0;
        
        private Element current;
        private Element next;
        private Element previous;

        @Override
        public boolean hasNext() {
            if (counter < size && index < elements.length) {
                return true;
            } else {
                return next != null;
            }
        }

        @Override
        public Element next() {

            if (index > elements.length) {
                throw new IndexOutOfBoundsException("iterating past the size of the map");
            }

            previous=current;
            
            if (next == null) {
                while (((current = elements[index]) == null)) {
                    if (index < elements.length) {
                        index++;
                    } else {
                        break;
                    }
                }
                if (current != null) {
                    if (current.next != null) {
                        next = current.next;
                    } else {
                        index++; //update the index only when there is no chained element in the same bucket (for this index)
                    }
                }
            } else {
                current = next;
                next = next.next;
            }

            counter++;
            return current;
        }

        @Override
        public void remove() {
            if (next == null) {
                
                if(current==null) { // this can happen only when the user called remove() before/without calling next()
                    throw new IllegalStateException("remove() called before/without calling next()");
                }
                
                if (current.next != null) {
                    elements[index] = current.next;
                    next = current.next;
                } else {
                    elements[index] = null;
                }
            } else {
                previous.next = next.next;
                next = next.next;
            }
            size--;
            
            resizeIfNeededForRemove(1);
        }
    }

    private final class KeyIterator implements Iterator<K> {

        private final ElementIterator iterator = new ElementIterator();

        @Override
        public boolean hasNext() {
            return iterator.hasNext();
        }

        @Override
        public K next() {
            K key = null;
            Element e = iterator.next();
            if (e != null) {
                key = e.key;
            }
            return key;
        }

        @Override
        public void remove() {
            iterator.remove();
        }

    }

    private final class ValueIterator implements Iterator<V> {

        private final ElementIterator iterator = new ElementIterator();

        @Override
        public boolean hasNext() {
            return iterator.hasNext();
        }

        @Override
        public V next() {
            V value = null;
            Element e = iterator.next();
            if (e != null) {
                value = e.value;
            }
            return value;
        }

        @Override
        public void remove() {
            iterator.remove();
        }

    }

}
