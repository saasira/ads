/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.unurs.ads;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * @author Samba Kolusu
 * @email saasira@live.com
 * @email saasira@gmail.com
 **/
public class Deadlock {

    private static final CountDownLatch latch = new CountDownLatch(2);
    private static final Lock one = new ReentrantLock();
    private static final Lock two = new ReentrantLock();

    public static class One extends Thread {

        @Override
        public void run() {
            try {

                System.out.println("attempt to aquire first lock by : " + this.getName());
                one.lock();
                System.out.println("first lock acquired by : " + this.getName());

                latch.await(100, TimeUnit.MILLISECONDS); 
                
                System.out.println("attempt to aquire second lock by : " + this.getName());
                two.lock();
                System.out.println("second lock acquired by : " + this.getName());

            } catch (InterruptedException ex) {
                ex.printStackTrace(System.out);
            } finally {
                one.unlock();
                latch.countDown();
            }
        }

    }

    public static class Two extends Thread {

        @Override
        public void run() {
            try {

                System.out.println("attempt to aquire second lock by : " + this.getName());
                two.lock();
                System.out.println("second lock acquired by : " + this.getName());

                latch.await(100, TimeUnit.MILLISECONDS);
                
                System.out.println("attempt to aquire first lock by : " + this.getName());
                one.lock();
                System.out.println("first lock acquired by : " + this.getName());
                
            } catch (InterruptedException ex) {
                ex.printStackTrace(System.out);
            } finally {
                two.unlock();
                latch.countDown();
            }
        }

    }

    public static void main(String... clps) {
        One first = new One();
        Two second = new Two();

        first.start();
        second.start();
    }
}
