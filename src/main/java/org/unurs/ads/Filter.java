/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.unurs.ads;

/**
 * @author Samba Kolusu
 * @email saasira@live.com
 * @email saasira@gmail.com
 **/
public interface Filter {
    
    /**
     * @param o input object on which the filtering criteria is to be applied
     * @return true if the object matches the filtering criteria, false otherwise
     **/
    public boolean apply(Object o);
    
}
