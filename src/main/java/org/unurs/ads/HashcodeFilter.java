/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.unurs.ads;

/**
 * @author Samba Kolusu
 * @email saasira@live.com
 * @email saasira@gmail.com
 **/
public class HashcodeFilter implements Filter {
    
    /**
     * @inheirtDoc
     **/
    @Override
    public boolean apply(Object o) {
        if(o==null) {
            return false;
        }
        return o.hashCode() > Short.MAX_VALUE;
    }
    
}
